Stormbeard's Random Dungeon Generator v0.01

This is a plugin that will randomly generate a dungeon map and provide ways to
control things like how many levels, bosses, treasure, and troops.

The flow of the map will be the player is ported into a random location and
then must defeat a boss monster in order * to open a portal that will spawn
elsewhere in the dungeon where the player will either progress deeper into the
dungeon or be ported to a user specified map.

This plugin also has dependencies on Yanfly's Event Chase Player plugin and it
must be included in the project.

Default Settings:
-----------------------------

The default settings will control how the dungeon is built and populated

Current Depth - Starting level of the dungeon to port to [usually 1]

Max Depth - How many total levels is the dungeon
            before porting to specified map

Switch Gate - A dedicated global switch # the plugin needs for gate management

Switch Boss - A dedicated global switch # the plugin needs for boss management

Dungeon Width - Tile width of the dungeon

Dungeon Height - Tile height of the dungeon

Dungeon Fullness - Ratio to control amount of walkable space

Room Size - Ratio to control the size of rooms

Random Encounters - Controls on map or random encounter for monsters

Monster Density - For random this is number of steps, for on map it roughly
                  how many square tiles per monster

Chest Density - How many square tiles per chest

Tileset ID - The ID of what tileset you've configure for the plugin

BGM - Background music name

Calling a Dungeon
-----------------------------

Using the Plugin Command event you can enter a string like
below to call a dungeon::

procgen mapgen dungeonname:dungeon1 outx:4 outy:6 outid:4

Breaking down the pieces of the command it looks like:

procgen - Name of the plugin, required [this never changes]

mapgen - Type of dungeon generation
         [currently only mapgen supported, more coming soon], required

dungeonname:XXX - Name of the Dungeon, this allows you to have
                  multiple different dungeons, required

outX:X outY:X outid:X - The output coordinates and mapID to send the player
                        at the completion of the dungeon, required



This is a list of optional parameter mappings you can use to override
the global defaults from the plugin manager to help customize every
dungeon the way you want it:

Plugin Command - Default Setting

start:X - Current Depth

max:X - Max Depth

gateswitch:X - Switch Gate

bossswitch:X - Switch Boss

w:X - Dungeon Width

h:X - Dungeon Height

fullness:X - Dungeon Fullness

roomsize:X - Room Size

userandom:X - Random Encounters

encounterrate:X - Monster Density

chestrate:X - Chest Density

tilseset:X - Tileset ID

bgm:X - BGM

So other examples:

A dungeon 100 tiles in height with more chests and defaults:

procgen mapgen dungeonname:dungeon1 outx:4 outy:6 outid:4 h:100 chestrate:500

A dungeon with less walkable space and defaults:

procgen mapgen dungeonname:dungeon1 outx:4 outy:6 outid:4 fullness:.4

A dungeon 3 levels deep using tileset 8 and defaults:

procgen mapgen dungeonname:dungeon1 outx:4 outy:6 outid:4 max:3 tileset:8

Adding Bosses and Monsters
-----------------------------

To Control what encounters a player will receive on a level you must pick
a troops and give it a custom name like the following:

procgen:dungeon1:troop:1:3:Monster:2:1:0:Bat*2

The command is broken down as follows:

procgen - never changes and should always be there

dungeon1 - You would replace this with your dungeonname
           param from the Plugin Command

troop - there are 2 values here, "troop" or "boss", troops are roaming
        monsters while boss will be a level boss for a floor

1:3 - this is the start level and end level of the dungeon a troop/boss is
      eligible for, ex if this was a 5 floor dungeon this troop would not
      appear on floors 4 or 5, if the value was 1:1 it would not appear on
      floors 2, 3, 4, or 5

Monster - This is the name of the character's sheet for the on map sprite

2:1:0 - direction, pattern, and character index of an on map monster - this
        controls which sprite of the sheet to use, but is best explained
        visually so I recommend looking at the online instructions at
        www.tbd.com

Bat*2 - A name of you choose to identify the troop, nothing to
        do with the plugin

Some examples you can use to get started:

procgen:dungeon1:troop:1:2:Monster:2:1:0:troop1

[Will show a bat on map]

procgen:dungeon1:troop:1:3:Monster:2:1:1:troop2

[Will show a slime on map]

procgen:dungeon1:boss:1:2:$BigMonster1:6:1:0:boss1

[Will use a big monster at the boss]

procgen:dungeon1:boss:3:3:$BigMonster1:2:1:0:boss2

[Will use a different big monster as the boss]

The above assumes you have the default resources in your game, if you apply
this to 4 troops the experience would be in a 3 floor dungeon you would fight
a mix of bat and slimes on floors 1 and 2 and fight boss 1 on both floors 1
and 2 on floor 3 you would only fight slime on the map and fight boss 2 at the
end.

One important note is that if you do not use on map monster's these value for
troops is irrelevant and you can set anything you would like there, but for
bosses is still important as they always appear on map.

Adding Treasure
-----------------------------

In the notes section of any item, armor, or weapon you can add a tag like the
following to add it to the dungeon:

  &lt;procgen 1:3:dungeon1 &gt;

A breakdown of the tag is:

&lt;procgen - always needed

1:3:dungeon1 - the start floor and end floor where the item can appear, and
               the dungeonname from the Plugin Command the item is tied to

 &gt; - always needed

So ex putting the above on a potion in a 5 floor dungeon the potion would have
a chance to appear on floors 1, 2, 3 but not on floors 4 or 5.

Currently to distribute treasure the plugin will look at how many possible
chest for the map level, then look at what items are available on the current
floor.  Each notetag is only good for 1 occurrence of an item on any given
floor, so ex if you put:

  &lt;procgen 1:3:dungeon1 &gt;
  
  &lt;procgen 1:1:dungeon1 &gt;
  
  &lt;procgen 1:1:dungeon1 &gt;

All on the potion item, it would mean that in a dungeon you could possibly
collect up to 3 potions on floor 1, and possibly collect one potion each on
both floors 2 and 3.

 if you just had:

  &lt;procgen 1:3:dungeon1 &gt;

and the player collects a potion on floor 1, there is still a possibility
that the player will collect another potion on both floor's 2 and 3,
meaning that each notetag does not map to being just 1 item received.

One other item to note is that currently the plugin will also add 1 gold chest
to the pool of available treasure for each map level.  It calculates the
average gold value of all possible items that could be received on that level
to determine value. So ex if you have 3 items note tagged for a level the gold
chest will function as a 4th available item, so if you want to ensure a player
gets all available items on any given level make sure you change you chest
density for 1 more chest than you notetags.  The other part of this to note is
that if you put something like only 1 very expensive item on a level that
there will be a corresponding large chest of gold as the only item available
to average off of is that very expensive item.  The gold chest system is
something that is actively being worked on and any recommendations are welcome.

Tilesets
-----------------------------

The plugin is driven off of a very specific typeset layout and it is best to
see this visually at www.tbd.com

Coming Soon/Next
-----------------------------
*Generating the map ina  more cavelike way vs corridors and rooms
*Refining the gold chest system
*Code Cleanup
*Simplifying inputs for troops
*Refining the item placement/generation system